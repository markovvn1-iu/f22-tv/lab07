<div align="center">
  <img src=".gitlab/logo.svg" height="80px"/><br/>
  <h1>Lab 7</h1>
  <p>Cloud Computing</p>
</div>



## 📝 Description

This project contains a solution to lab 7 in the Total Virtualisation course.

<img src=".gitlab/gitlab.svg" height="18px" /> Gitlab link: https://gitlab.com/markovvn1-iu/f22-tv/lab07

## Task

Compute cost for deploying the service that requires 64 servers (each server 8core, 32GB RAM) & 512TB of storage.

### Own

General formula: (Storage + Servers) + (sysadmin + energy cost (PUE=2.0)) * M

| Item name                 | Cost per item | Power required (W) | Count         |
| ------------------------- | ------------- | ------------------ | ------------- |
| 16 TB of HDD storage      | 448$          | 6                  | 30            |
| 3.84 TB of SSH storage    | 848$          | 8                  | 4             |
| 8 core server CPU         | 608$          | 100                | 64            |
| Motherboard               | 160$          | 30                 | 64            |
| 32 GB of RAM              | 128$          | 4                  | 64            |
| 1 month of sysadmin       | 1400$         | -                  | M             |
| 1 W of energy for 1 month | 0.05$         | -                  | 2 * (148) * M |

Total cost for owning the server will be around: 74000 + 1700 * M

## Rent (Yandex cloud)

General formula: (S3 x volume + EC2 (m4.x2large)) * M

| Item name                | Cost per item | Count  |
| ------------------------ | ------------- | ------ |
| 480 TB of object storage | 13560$        | 1 * M  |
| 32 TB of SSH storage     | 6240$         | 1 * M  |
| 8 core server CPU        | 90$           | 64 * M |
| 32 GB of RAM             | 110$          | 64 * M |

Total cost for ranting the server will be around: 20000 * M

## Rent (Google cloud)

| Item name                       | Cost per item | Count  |
| ------------------------------- | ------------- | ------ |
| 480 TB of object storage        | 9830$         | 1 * M  |
| 32 TB of SSH storage            | 9830$         | 1 * M  |
| 8 core server CPU, 32 GB of RAM | 195$          | 64 * M |

Total cost for ranting the server will be around: 32200 * M

## Conclusion

The cost of the internet is highly dependent on the number of transactions the service will carry out, which we do not know.

In the formula for rent, we did not include internet costs (as they are not known) and the salary of the system's setup man.

In the formula for ownership, we did not take into account the cost of the internet, nor did we include storage overhead to set up a RAID array for reliable data storage. We also did not include the salary of the person who selects all the components and the cost of renting the space.

Let's compare costs:

![](.gitlab/img01.png)

So, for Google Cloud M_crit = 2.5, for Yandex M_crit= 4. M_crit - the number of months after which having own infrastructure is cheaper.

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>